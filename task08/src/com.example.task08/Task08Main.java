package com.example.task08;
import java.util.Arrays;
public class Task08Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        int[] arr = new int[2];
        arr[0] = 8000000;
        arr[1] = 9000000;
        System.out.println(mult(arr));

    }

    static long mult(int[] arr) {
        if(Arrays.stream(arr).count() == 0)
            return 0;
        else
            return Arrays.stream(arr).asLongStream().reduce(1L, (a, b) -> a * b);
    }

}