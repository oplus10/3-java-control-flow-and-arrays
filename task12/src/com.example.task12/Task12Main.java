package com.example.task12;

import java.util.Arrays;

public class Task12Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        int[] arr = {9, 11, 7, 8};
        selectionSort(arr);
        System.out.println(java.util.Arrays.toString(arr));

    }

    static void selectionSort(int[] arr) {
        if (arr == null || arr.length == 0)
            return;

        for (int i = 0; i < arr.length - 1; i++)
            swap(arr, i);
    }

    static void swap(int[] arr, int start_index) {
        int[] tempArray = Arrays.copyOfRange(arr, start_index, arr.length);
        int min_element = Arrays.stream(tempArray).min().getAsInt();
        for (int i = start_index; i < arr.length; i++) {
            if (arr[i] == min_element) {
                arr[i] = arr[start_index];
                arr[start_index] = min_element;
                return;
            }
        }

    }

}