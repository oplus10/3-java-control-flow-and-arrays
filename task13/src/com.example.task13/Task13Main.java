package com.example.task13;

public class Task13Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {9, 1100, 7, 8};
        removeMoreThen1000(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static int[] removeMoreThen1000(int[] arr) {
        if(arr == null || arr.length == 0)
            return arr;

        int[] new_arr = new int[arr.length];
        int new_arr_index = 0;
        for (int i : arr) {
            if (i <= 1000) {
                new_arr[new_arr_index] = i;
                new_arr_index++;
            }
        }
        int[] result_arr = new int[new_arr_index];
        System.arraycopy(new_arr, 0, result_arr, 0, new_arr_index);
        return result_arr;
    }

}